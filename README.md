# 4thNigma - An Enigma Simulator written in Forth

This project written in (G)Forth simulates an [Enigma](https://en.wikipedia.org/wiki/Enigma_machine) cipher machine.

At the current state, it is possible to simulate an Enigma M3, with 8 rotors available. Further rotors can be added to [rotor_definitions.fs](./rotor_definitions.fs).

For detailed encryption steps, use [enigma_verbose.fs](./enigma_verbose.fs) instead of [enigma.fs](./enigma.fs).


## Dependencies

* mini-oof.fs (minimal object oriented library)

## Usage

    require rotor_definitions.fs
    require enigma.fs

    \ define new objects as constants
    plugboard new constant pb
    enigma new constant enigma_III

    \ initialize plugboard
    "bq cr di ej kw mt os px uz gh" pb pb_init

    \ setup enigma by specifying the rotors, initial rotor position, ring settings and the plugboard
    ukw_B rot_IV rot_I rot_III 'A' 'Q' 'L' 1 2 3 pb enigma_III init_enigma

    \ encrypt the string "hello forth"
    cr "hello forth" enigma_III encstr

    \ expected output: KNSVS CFCAU
