require enigma.fs

enigma class
end-class enigma_verbose


:noname ( c o -- c) >r
    dup emit ."  -> " left  r@ r3  @ encode
    dup emit ."  -> " left  r@ r2  @ encode
    dup emit ."  -> " left  r@ r1  @ encode
    dup emit ."  -> " left  r@ ukw @ encode
    dup emit ."  -> " right r@ r1  @ encode
    dup emit ."  -> " right r@ r2  @ encode
    dup emit ."  -> " right r> r3  @ encode ;
enigma_verbose defines do_encoding


:noname ( c o -- c ) >r
    toupper
    dup r@ [ enigma :: do_rotation ]
    r@ [ enigma :: print_positions ]
    dup isletter if
        r> do_encoding
    else
        r> drop
    endif ;
enigma_verbose defines encchar


:noname { addr u o }
    u 0 u+do
        addr i chars + c@
        dup emit ." -> "
        o encchar emit cr
    loop ;
enigma_verbose defines encstr
