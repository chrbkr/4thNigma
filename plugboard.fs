require mini-oof.fs
require util.fs

object class
    cell var pb_mapping
    method pb_init
    method pb_encode
end-class plugboard

:noname { addr u o }
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ" drop o pb_mapping !
    0 begin dup u < while
        dup chars addr + c@
        toupper dup isletter if
            swap 1+ dup chars addr + c@ toupper rot
            2dup swap
            tonumber chars o pb_mapping @ + c!
            tonumber chars o pb_mapping @ + c!
        else
            drop
        endif
        1+
    repeat
    drop ;
plugboard defines pb_init

:noname ( c o -- c ) >r
    dup isletter if
        tonumber chars r> pb_mapping @ + c@
    else
        r> drop
    endif ;
plugboard defines pb_encode
