require mini-oof.fs
require plugboard.fs
require rotor.fs

object class
    cell var ukw
    cell var r1
    cell var r2
    cell var r3
    cell var plug_board
    method init_enigma
    method encchar
    method encstr
    method do_rotation
    method do_encoding
    method print_positions
end-class enigma

:noname { rotor_ukw rotor1 rotor2 rotor3 pos1 pos2 pos3 ring1 ring2 ring3 pb o }
    rotor_ukw o ukw !
    pos1 ring1 rotor1 config rotor1 o r1 !
    pos2 ring2 rotor2 config rotor2 o r2 !
    pos3 ring3 rotor3 config rotor3 o r3 !
    pb o plug_board ! ;
enigma defines init_enigma


:noname ( c o -- ) >r
    isletter if
        r@ r3 @ rotate              \ r3 always rotates
        r@ r2 @ at_notch or if      \ r2 rotates if r3 stepped over notch OR if r2 is at notch
            r@ r2 @ rotate if
                r@ r1 @ rotate drop \ r1 rotates if r2 stepped over notch
            endif
        endif
    endif
    r> drop ;
enigma defines do_rotation


:noname ( c o -- c) >r
    r@ plug_board @ pb_encode 
    left  r@ r3  @ encode
    left  r@ r2  @ encode
    left  r@ r1  @ encode
    left  r@ ukw @ encode
    right r@ r1  @ encode
    right r@ r2  @ encode
    right r@ r3  @ encode
    r> plug_board @ pb_encode ;
enigma defines do_encoding


:noname ( c o -- c ) >r
    dup r@ do_rotation
    r> do_encoding ;
enigma defines encchar


:noname { addr u o }
    0
    u 0 u+do
        addr i chars + c@
        dup isletter if
            o encchar emit
            1+ dup 5 mod 0 = if
                bl emit
            endif
        else
            drop
        endif
    loop drop ;
enigma defines encstr


:noname ( o -- ) >r
    ." ("
    r@ r1 @ position @ emit
    r@ r2 @ position @ emit
    r> r3 @ position @ emit
    ." ) " ;
enigma defines print_positions
