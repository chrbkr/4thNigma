require mini-oof.fs
require util.fs

object class
    cell var mapping
    cell var notch
    cell var notch_count
    cell var position
    cell var ring
    method init
    method config
    method encode
    method rotate
    method to_index
    method curr_offset
    method at_notch
end-class rotor


:noname { map_addr map_u notch_addr notch_u o }
    'A' o position !
    1 o ring !

    notch_addr o notch !
    notch_u o notch_count !

    here 26 allot
    o mapping !

    map_u 0 u+do
        map_addr i + c@
        tonumber
        o mapping @ i +
        c!
    loop ;
rotor defines init


:noname ( c u o -- ) >r
    r@ ring !
    r> position ! ;
rotor defines config


: left true ;
: right false ;

:noname ( c b o -- c )
    { c dir o }
    c toupper
    dup isletter if
        tonumber
        o curr_offset + 26 mod
        o mapping @
        dir if
            + c@
        else
            26 rot indexof_n
        endif
        o curr_offset -
        toletter
    endif ;
rotor defines encode


:noname ( o -- u ) >r
    r@ position @ tonumber
    r> ring @ 1- - ;
rotor defines curr_offset


:noname ( o -- ) >r
    r@ at_notch
    r@ position @
    1+ normalize
    r> position ! ;
rotor defines rotate

:noname ( o -- b ) >r
    r@ notch @ r@ notch_count @
    r> position c@
    indexof 0<= ;
rotor defines at_notch
