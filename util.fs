: isletter ( c -- flag )
    toupper
    dup 'A' >=
    swap 'Z' <=
    and ;


: indexof ( addr u c -- u )
    >r dup -rot r> scan nip - ;


: indexof_n ( addr u n -- u )
    -rot dup -rot
    chars +
    swap
    begin
        1- >r
        1 chars -
        dup c@
        rot tuck
        <> r@ 0> and
        while
            r> swap -rot
        repeat
        2drop
        r> ;


: tonumber ( c -- u )
    toupper
    'A' - ;


: toletter ( u -- c)
    26 mod
    'A' + ;


: normalize ( c -- c )
    tonumber toletter ;
